// main.qml

import QtQuick 2.3
import QtQuick.Window 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2


ApplicationWindow {
    visible: true

    property bool isDevice: Qt.platform.os == "android" || Qt.platform.os == "ios" || Qt.platform.os == "blackberry"

    width: isDevice ? Screen.desktopAvailableWidth : /*isDesktop*/ 400
    height: isDevice ? Screen.desktopAvailableHeight : /*isDesktop*/ 600
    title: qsTr("Atlz QtQuick")

    menuBar: MenuBar {
        Menu {
            title: qsTr("Menu")
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    Item {
        id: root
        anchors.fill: parent
        state: "Ready"

        states: [
            State {
                name: "Ready"
                PropertyChanges { target: btnStartCalculation; enabled: true; }
                PropertyChanges { target: lblState; visible: false; }
                PropertyChanges { target: busyIndicator; opacity: 0; }
            },
            State {
                name: "Counting"
                PropertyChanges { target: btnStartCalculation; enabled: false; }
                PropertyChanges { target: lblState; visible: true;}
                PropertyChanges { target: busyIndicator; opacity: 1; }
            },
            State {
                name: "Show results"
                PropertyChanges { target: btnStartCalculation; enabled: false; }
                PropertyChanges { target: lblState; visible: true; }
                PropertyChanges { target: busyIndicator; opacity: 0; }
            }
        ]

        // Button to start calcutalion
        Button {
            id: btnStartCalculation
            text: "Count"
            anchors.centerIn: parent
            onClicked: {
                root.state = "Counting"
                counterOfLuckyTickets.sendMessage()
                root.state = "Show results"
            }
        }

        // Script that will count the number of lucky tickets.
        WorkerScript {
            id: counterOfLuckyTickets
            source: "./counterOfLuckyTickets.js"
            onMessage: lblState.text = messageObject.reply
        }

        // Label with states
        Text {
            id: lblState
            anchors {
                bottom: btnStartCalculation.top
                horizontalCenter: btnStartCalculation.horizontalCenter
                bottomMargin: Screen.pixelDensity
            }
            text: "Counting"
        }

        // Busy indicator. Displayed while calculation
        BusyIndicator{
            id: busyIndicator
            running: true
            anchors {
                bottom: lblState.top
                horizontalCenter: lblState.horizontalCenter
            }

            Behavior on opacity { PropertyAnimation { duration: 500 } }
        }
    }
}
